# SOWA

Stochastic Optimizer Web Application (SOWA) is a Web Application that allows an
user-friendly estimation of parameters and dataset generation. 

## Requirements

It has been tested on Ubuntu 20.04, but should work in any GNU/Linux with the utilities below:

* [Flask](https://flask.palletsprojects.com) - Application framework
* [SQLite 3](https://www.sqlite.org) - RDBMS
* [SQLAlchemy](https://www.sqlalchemy.org/) - Object-relational mapping tool


## Installation

1. Get the code from [GitLab](https://gitlab.com/rpizziol/sowa) or directly
clone it from the git repository:
```
git clone https://gitlab.com/rpizziol/sowa.git
```
...

## Usage
...

## Versioning
For the versions available, see the [tags on this repository](https://gitlab.com/rpizziol/sowa/-/tags). 

## Authors
* **Roberto Pizziol** - *SOWA web application* - [rpizziol](https://gitlab.com/rpizziol/)
