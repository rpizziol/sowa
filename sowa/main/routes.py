from flask import Blueprint
from flask import render_template, request
from flask_login import current_user
from sqlalchemy import or_, not_

from sowa.models import Post

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    """
    Homepage of the website.
    :return:
    """
    return render_template("main/home.html", title='Homepage', legend='Homepage Sowa')


@main.route("/history")
def history():
    page = request.args.get('page', default=1, type=int)
    # Take all public posts + private posts of the current_user (if authenticated)
    if current_user.is_authenticated:
        query = Post.query.filter(or_(Post.author == current_user, not_(Post.is_private)))
    else:
        query = Post.query.filter(not_(Post.is_private))
    posts = query.order_by(Post.date_posted.desc()).paginate(per_page=5, page=page)
    return render_template("main/history.html", posts=posts)
