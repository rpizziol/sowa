from datetime import datetime, timezone, timedelta

from flask import current_app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from sowa import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(16), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    image_file = db.Column(db.String(32), nullable=False, default='default.jpg')
    password = db.Column(db.String(64), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)

    def get_reset_token(self, expires_sec=1800):  # 1800 seconds = 30 minutes
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    def verify_reset_token(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)  # CET timezone datetime.now(timezone(timedelta(hours=2)))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    p = db.Column(db.Integer, nullable=False)
    q = db.Column(db.Integer, nullable=False)
    result = db.Column(db.String(128), nullable=False)
    sigma2 = db.Column(db.Float, nullable=False)
    stderr = db.Column(db.Float, nullable=False)
    title = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(256), nullable=True)
    is_private = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"Post('id:{self.id}', 'ARMA({self.p}, {self.q})', '{self.date_posted}', '{self.result}')"
