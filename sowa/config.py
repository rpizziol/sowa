import os


class Config:
    # Note: add those two lines to '~/.bashrc' to use the environment variables
    # export SECRET_KEY='#### Secret Key here ####'
    # export SQLALCHEMY_DATABASE_URI='#### database URI here ####'
    # export EMAIL_USER="your@email.here"
    # export EMAIL_PASS="#### Your Email Password Here ####"
    SECRET_KEY = os.environ.get('SECRET_KEY')

    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_USERNAME = os.environ.get('EMAIL_USER')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True

    PLOTS_FOLDER = 'sowa/static/plots/'
    DATASETS_FOLDER = 'sowa/static/datasets/'
