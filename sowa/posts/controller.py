from secrets import token_hex

from flask import flash, abort
from flask_login import current_user
from numpy import array, float, savetxt

from sowa.posts.utils import simulate_arma_data, evaluate_fitting
from sowa import db
from sowa.models import Post
from sowa.config import Config


def delete_post_controller(post_id):
    post = Post.query.get_or_404(post_id)  # If it doesn't exist: error 404
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Your post has been deleted!', 'success')
    return None


def generate_dataset_controller(form):
    # Parse the input
    try:
        ar_params_array = array(form.ar_params.data.split(",")).astype(float)
    except:
        ar_params_array = array([])
    try:
        ma_params_array = array(form.ma_params.data.split(",")).astype(float)
    except:
        ma_params_array = array([])
    sim_data, p, q = simulate_arma_data(form.t_max.data, ar_params_array, ma_params_array,
                                        form.seed.data, form.variance.data)
    filename = token_hex(8)  # Generate a new filename
    savetxt(Config.DATASETS_FOLDER + filename + ".csv", sim_data, delimiter=",")
    return filename


def estimations_evaluator_controller(form):
    # Parse the input
    try:
        ar_params_array = array(form.ar_params.data.split(",")).astype(float)
    except:
        ar_params_array = array([])
    try:
        ma_params_array = array(form.ma_params.data.split(",")).astype(float)
    except:
        ma_params_array = array([])
    return evaluate_fitting(form.t_max.data, ar_params_array, ma_params_array, form.variance.data, form.n.data, form.library.data)
