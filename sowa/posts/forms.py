from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from numpy import r_, array
from statsmodels.api import tsa
from wtforms import StringField, SubmitField, FileField, IntegerField, FloatField, TextAreaField, BooleanField, \
    SelectField
from wtforms.validators import NumberRange, InputRequired, Regexp, ValidationError
from sowa.posts.utils import is_stationary, is_invertible


class DatasetForm(FlaskForm):
    """
    The form used to generate a custom ARMA dataset.
    """
    ar_params = StringField('AR parameters', validators=[
        Regexp(r'(^$|^(?:[+-]?\d+(?:\.\d*)?|\.\d+)(?:,(?:[+-]?\d+(?:\.\d*)?|\.\d+))*$)',
               message='Invalid input. Insert all coefficients separated by a comma (e.g. \"0.3,0.001\")')])
    ma_params = StringField('MA parameters', validators=[
        Regexp(r'(^$|^(?:[+-]?\d+(?:\.\d*)?|\.\d+)(?:,(?:[+-]?\d+(?:\.\d*)?|\.\d+))*$)',
               message='Invalid input. Insert all coefficients separated by a comma (e.g. \"0.3,0.001\")')])
    t_max = IntegerField('Number of samples', validators=[InputRequired(), NumberRange(min=100, max=10000)],
                         default=200)
    variance = FloatField('Variance', validators=[InputRequired(), NumberRange(min=0)], default=1)
    try:
        seed = IntegerField('Random seed', validators=[InputRequired(), NumberRange(min=0, max=4294967295)],
                            default=12345)  # TODO fix OverflowError for large inputs
    except OverflowError as e:
        raise Exception(e.args[0])
    submit = SubmitField('Create dataset')

    def validate_ar_params(self, ar_params):
        try:
            ar_params_array = array(ar_params.data.split(",")).astype(float)
        except:
            ar_params_array = array([])
        if not is_stationary(ar_params_array):
            raise ValidationError('The parameters selected generate a non-stationary process. Select other parameters.')
        # ar = r_[1, -ar_params_array]  # add zero-lag and negate
        # arma_process = tsa.ArmaProcess(ar=ar)
        # if not arma_process.isstationary:
        #     raise ValidationError('The parameters selected generate a non-stationary process. Select other parameters.')

    def validate_ma_params(self, ma_params):
        try:
            ma_params_array = array(ma_params.data.split(",")).astype(float)
        except:
            ma_params_array = array([])
        ma = r_[1, ma_params_array]  # add zero-lag
        arma_process = tsa.ArmaProcess(ma=ma)
        if not arma_process.isinvertible:
            raise ValidationError('The parameters selected generate a non-invertible process. Select other parameters.')

    def validate(self, extra_validators=None):
        """Custom validator to ensure at least on of the parameter fields is used."""
        if super().validate(extra_validators):
            if not (self.ar_params.data or self.ma_params.data):
                self.ar_params.errors.append('At least one of these fields must have a value.')
                self.ma_params.errors.append('At least one of these fields must have a value.')
                return False
            else:
                return True
        return False


class EvaluateEstimationsForm(FlaskForm):
    """
    The form used to evaluate the performances of the parameter estimation tool.
    """
    ar_params = StringField('AR parameters', validators=[
        Regexp(r'(^$|^(?:[+-]?\d+(?:\.\d*)?|\.\d+)(?:,(?:[+-]?\d+(?:\.\d*)?|\.\d+))*$)',
               message='Invalid input. Insert all coefficients separated by a comma (e.g. \"0.3,0.001\")')])
    ma_params = StringField('MA parameters', validators=[
        Regexp(r'(^$|^(?:[+-]?\d+(?:\.\d*)?|\.\d+)(?:,(?:[+-]?\d+(?:\.\d*)?|\.\d+))*$)',
               message='Invalid input. Insert all coefficients separated by a comma (e.g. \"0.3,0.001\")')])
    t_max = IntegerField('Number of samples', validators=[InputRequired(), NumberRange(min=100, max=10000)],
                         default=200)
    library = SelectField(label='Library', choices=[('sm', 'Statsmodels (recommended)'), ('cm', 'Experimental')])
    n = IntegerField('Number of tests', validators=[InputRequired(), NumberRange(min=10, max=100)], default=30)

    variance = FloatField('Variance', validators=[InputRequired(), NumberRange(min=0)], default=1)
    submit = SubmitField('Evaluate')

    def validate_ar_params(self, ar_params):
        try:
            ar_params_array = array(ar_params.data.split(",")).astype(float)
        except:
            ar_params_array = array([])
        if not is_stationary(ar_params_array):
            raise ValidationError('The parameters selected generate a non-stationary process. Select other parameters.')

        # ar = r_[1, -ar_params_array]  # add zero-lag and negate
        # arma_process = tsa.ArmaProcess(ar=ar)
        # if not arma_process.isstationary:
        #     raise ValidationError('The parameters selected generate a non-stationary process. Select other parameters.')

    def validate_ma_params(self, ma_params):
        try:
            ma_params_array = array(ma_params.data.split(",")).astype(float)
        except:
            ma_params_array = array([])
        if not is_invertible(ma_params_array):
            raise ValidationError('The parameters selected generate a non-invertible process. Select other parameters.')
        # ma = r_[1, ma_params_array]  # add zero-lag
        # arma_process = tsa.ArmaProcess(ma=ma)
        # if not arma_process.isinvertible:
        #     raise ValidationError('The parameters selected generate a non-invertible process. Select other parameters.')

    def validate(self, extra_validators=None):
        """Custom validator to ensure at least on of the parameter fields is used."""
        if super().validate(extra_validators):
            if not (self.ar_params.data or self.ma_params.data):
                self.ar_params.errors.append('At least one of these fields must have a value.')
                self.ma_params.errors.append('At least one of these fields must have a value.')
                return False
            else:
                return True
        return False


class AnalysisForm(FlaskForm):
    """
    The form used to perform a parameter estimation on a given dataset.
    """
    dataset = FileField('Upload Dataset', validators=[InputRequired(), FileAllowed(['csv'])])
    p = IntegerField('p', validators=[InputRequired(), NumberRange(min=0)], default=0)
    q = IntegerField('q', validators=[InputRequired(), NumberRange(min=0)], default=0)
    is_private = BooleanField('Private')
    title = StringField('Title', validators=[])
    description = TextAreaField('Description')
    library = SelectField(label='Library', choices=[('sm', 'Statsmodels (recommended)'), ('cm', 'Experimental')])
    submit = SubmitField('Run')

    def validate(self, extra_validators=None):
        """Custom validator to ensure at least on of the parameter fields is used."""
        if super().validate(extra_validators):
            if self.p.data == 0 and self.q.data == 0:
                self.p.errors.append('Those fields cannot be 0 at the same time')
                self.q.errors.append('Those fields cannot be 0 at the same time')
                return False
            else:
                return True
        return False
