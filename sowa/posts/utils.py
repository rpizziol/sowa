import math
import os
from secrets import token_hex

import matplotlib.pyplot as plt
import sympy as sp

from math import sqrt

from flask_login import current_user
import numpy as np
from scipy.optimize import minimize

from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.api import tsa
import seaborn as sns

from sowa.models import Post, User
from sowa import db, Config, create_app


def simulate_arma_data(n_sample, ar_params, ma_params, seed, variance):
    """
    Function that simulates an ARMA process.
    :param variance:
    :param seed:
    :param n_sample:    Number of samples to be generated
    :param ar_params:   The AR parameters (phi_1, phi_2...)
    :param ma_params:   The MA parameters (theta_1, theta_2...)
    :return:            An array of simulated data, p and q
    """
    np.random.seed(seed)
    p = len(ar_params)
    q = len(ma_params)
    ar = np.r_[1, -ar_params]  # add zero-lag and negate
    ma = np.r_[1, ma_params]  # add zero-lag
    # print("+++ Simulated Process +++")
    # print("p =", p, ", q =", q)
    # print("AR params =", ar_params)
    # print("MA params =", ma_params)
    arma_process = tsa.ArmaProcess(ar=ar, ma=ma)
    # simulated_arma_process = tsa.arma_generate_sample(ar, ma, n_sample)
    # if arma_process.isstationary:
    #     print('The process is stationary.')
    # else:
    #     print('The process is NOT stationary.')
    # if arma_process.isinvertible:
    #     print('The process is invertible.')
    # else:
    #     print('The process is NOT invertible.')
    simulated_data = arma_process.generate_sample(nsample=n_sample, scale=sqrt(variance))
    return simulated_data, p, q


def generate_arma_plot_file(data, p, q, filename):
    """
    Generate and save a png containing the plot of a given dataset.
    :param data:    An array containing the dataset
    :param p:       The 'p' of the AR part
    :param q:       The 'q' of the MA part
    :param filename:The name of the dataset file (without extension)
    :return:        The name of the newly generated file
    """
    # Display the process
    plt.style.use('seaborn')
    plt.figure(figsize=[10, 7.5])
    plt.plot(data)
    plt.title("#" + str(filename) + " - ARMA(" + str(p) + " ," + str(q) + ") Process")
    plt.savefig(Config.PLOTS_FOLDER + filename)
    # Partial Autocorrelation plot
    plot_pacf(data)
    plt.title(f"#{filename} - Partial Autocorrelation")
    plt.savefig(Config.PLOTS_FOLDER + filename + '-pacf')
    # Autocorrelation plot
    plot_acf(data)
    plt.title(f"#{filename} - Autocorrelation")
    plt.savefig(Config.PLOTS_FOLDER + filename + '-acf')

    return filename + '.png'


def is_stationary_and_invertible(psi0, p, q):
    # ar_lambda = np.r_[1, -psi0[0:p]]  # add zero-lag and negate
    # ma_lambda = np.r_[1, psi0[p:p + q]]  # add zero-lag
    #
    # # Stationarity test
    # ar_lambda_roots = np.roots(ar_lambda)
    # for i in range(p):
    #     if np.absolute(ar_lambda_roots[i]) >= 1:
    #         return False
    #
    # # Invertibility test
    # ma_lambda_roots = np.roots(ma_lambda)
    # for i in range(q):
    #     if np.absolute(ma_lambda_roots[i]) >= 1:
    #         return False
    # return True
    #
    #
    return is_stationary(psi0[0:p]) and is_invertible(psi0[p:p + q])


def is_stationary(phi):
    ar_lambda = np.r_[1, -phi]  # add zero-lag
    ar_lambda_roots = np.roots(ar_lambda)
    for root in ar_lambda_roots:
        if np.absolute(root) >= 1:
            return False
    return True


def is_invertible(theta):
    ma_lambda = np.r_[1, theta]  # add zero-lag
    ma_lambda_roots = np.roots(ma_lambda)
    for root in ma_lambda_roots:
        if np.absolute(root) >= 1:
            return False
    return True


# def ar_state_space_model(psi, p, q):
#     """
#     - State Space Model -
#     Obtain the matrices F, G, Q and R of the ARMA state space model.
#     :param psi: The parameter array (phi and theta values)
#     :param p:   The order 'p' of the AR process
#     :param q:   The order 'q' of the MA process
#     :return:    The matrices F, G, Q and R
#     """
#
#     F = np.eye(p, k=-1)
#     for i in range(p):
#         F[0, i] = psi[i]
#
#     G = np.zeros((1, p))
#     G[0, 0] = 1
#
#     T = np.zeros((p, q + 1))
#     T[0, 0] = 1
#     for i in range(q):
#         T[0, i + 1] = psi[p + i]  # The first [0, p-1] elements are occupied by phi
#
#     Q = T.dot(T.transpose())
#
#     R = np.zeros(1)
#     return F, G, Q, R


# # In case the ARMA process is just a MA process (p = 0) it is more convenient to use this SSM representation
# def ma_state_space_model(theta, q):
#     F = np.eye(q, k=-1)
#     G = np.zeros((1, q))
#     for i in range(q):
#         G[0, i] = theta[i]
#     T = np.zeros((q, 1))
#     T[0, 0] = 1
#     Q = T.dot(T.transpose())
#     R = np.eye(1)
#     return F, G, Q, R


def ma_state_space_model(theta, q):
    F = np.eye(q + 1, k=-1)

    G = np.zeros((1, q + 1))
    G[0, 0] = 1
    for i in range(q):
        G[0, i + 1] = theta[i]

    T = np.zeros((q + 1, 1))
    T[0, 0] = 1
    Q = T.dot(T.transpose())

    R = 0

    return F, G, Q, R


def arma_state_space_model(psi, p, q):
    rmax = max(p, q + 1)

    F = np.eye(rmax, k=1)
    for i in range(p):
        F[i, 0] = psi[i]

    G = np.zeros((1, rmax))
    G[0, 0] = 1

    T = np.zeros((rmax, 1))
    T[0, 0] = 1
    for i in range(q):
        T[i + 1, 0] = psi[p + i]
    Q = T.dot(T.transpose())

    R = np.zeros(1)

    return F, G, Q, R


def kalman_filter(t_max, F, G, Q, R, Y):
    """
    Notice that since the case we are considering is univariate (N = 1), we can simplify the code as follows:
    - np.linalg.inv(H[t]) = (1 / H[1])
    :param t_max:   The size of the dataset available
    :param F:       The transition process in the SSM
    :param G:       The design process in the SSM
    :param Q:       The variance of V_t in the SSM
    :param R:       The variance of W_t in the SSM
    :param Y:       The observed dataset
    :return:        Two arrays containing all the values of H_t (the state innovation covariance) and v_t
    """
    # Initialization
    dimM = np.shape(F)[0]
    H = [None] * (t_max + 1)
    K = [None] * (t_max + 1)
    P = [None] * (t_max + 1)
    P[0] = stationary_initialization(F, Q)
    # P[0] = np.eye(dimM)
    # print(P[0])
    hatX = [None] * (t_max + 1)
    hatX[0] = np.zeros((dimM, 1))  # mu_0 (initial mean)
    v = [None] * (t_max + 1)
    for t in range(1, t_max + 1):
        H[t] = G.dot(F.dot(P[t - 1].dot(F.transpose().dot(G.transpose())))) + G.dot(Q.dot(G.transpose())) + R
        K[t] = F.dot(P[t - 1].dot(F.transpose())).dot(G.transpose()) * 1 / H[t] + Q.dot(G.transpose()) * 1 / H[t]
        hatX[t] = (F - K[t].dot(G.dot(F))).dot(hatX[t - 1]) + K[t].dot(Y[t - 1])
        P[t] = (F - K[t].dot(G.dot(F))).dot(P[t - 1]).dot(F.transpose()) + Q - K[t].dot(G.dot(Q))
        v[t] = Y[t - 1] - G.dot(F.dot(hatX[t - 1]))
    return H, v


def log_likelihood(H, v, t_max):
    """
    This function calculates the log-likelihood in the prediction error decomposition form as a function of the
    parameters psi (present in both the matrix H and the vector v).
    Notice that since the case we are considering is univariate (N = 1), we can simplify the code as follows:
    - np.linalg.inv(H[t]) = (1 / H[1])
    - np.linalg.det(H[1]) = H[1] .
    :param H:       An array of values of the state innovation covariance H_t calculated with the Kalman filter
    :param v:       An array of values of the vector v_t (that depends on the observed dataset)
    :param t_max:   T, the size of the random sample
    :return:        The value of the prediction error decomposition
    """
    sum1 = np.log(H[1])
    sum2 = v[1].transpose().dot((1 / H[1]).dot(v[1]))
    for i in range(2, t_max):
        sum1 += np.log(H[i])
        # print(H[i])
        sum2 += v[i].transpose().dot((1 / H[i]).dot(v[i]))
    return (- ((1 * t_max) / 2) * np.log(2 * np.pi) - 0.5 * sum1 - 0.5 * sum2)[0, 0]


def neg_log_likelihood(psi, Y, p, q):
    """
    This function executes in order the main steps of the algorithm. Firstly it obtains the state space model from the
    given process (that is, the matrices F, G, Q, and R), then it executes the Kalman filter procedure to obtain the
    matrix H and the vector v, and, finally, it calculates the negative log-likelihood.
    :param psi: The array of parameters (phi and theta of ARMA)
    :param Y:   The input dataset
    :param p:   The order 'p' of the AR model
    :param q:   The order 'q' of the MA model
    :return:    The value of the negative log-likelihood.
    """
    if p == 0:  # MA process
        F, G, Q, R = ma_state_space_model(psi, q)
    else:  # ARMA / AR process
        F, G, Q, R = arma_state_space_model(psi, p, q)
    t_max = Y.size
    H, v = kalman_filter(t_max, F, G, Q, R, Y)
    # print(H)
    return -log_likelihood(H, v, t_max)


def maximize_likelihood(data, p, q, library):
    """
    This function works as an entry point for the estimation of the parameters.
    :param data:    The input dataset
    :param p:       The order 'p' of the AR model
    :param q:       The order 'q' of the MA model
    :param library: The library to use for the parameters' estimation
    :return:        The computed estimation of parameters and sigma
    """
    if library == 'sm':  # statsmodels
        arma_results = tsa.ARMA(data, (p, q)).fit(trend='nc', disp=0)
        opt_res = arma_results.params.round(3)
        opt_sigma = arma_results.sigma2.round(3)
    else:  # experimental
        while True:
            while True:
                psi0 = np.random.rand(p + q)  # random initialization
                if is_stationary_and_invertible(psi0, p, q):
                    break
            res = minimize(neg_log_likelihood, x0=psi0, method='nelder-mead', options={'xatol': 1e-10, 'disp': True},
                           args=(data, p, q))
            opt_res = res.x.round(3)
            if is_stationary_and_invertible(res.x, p, q):
                break
        opt_sigma = 1
    stde = round(std_err(data, p, opt_res, opt_sigma), 3)
    return opt_res, opt_sigma, stde


def std_err(data, p, psi, sigma):
    sim_data, _, _ = simulate_arma_data(data.size, psi[0:p - 1], psi[p:], np.random.randint(0, 100000), sigma ** 2)
    diff = data - sim_data
    return math.sqrt(np.sum(np.square(diff)) / data.size)


def execute_analysis(p, q, library, title, description, is_private, filename):
    # Obtain the dataset from the file
    dataset = np.genfromtxt(Config.DATASETS_FOLDER + filename, delimiter=',')
    # Optimize parameters
    result, sigma2, stde = maximize_likelihood(data=dataset, p=p, q=q, library=library)
    stringed_result = ','.join(str(x) for x in result)
    # Add results to the database
    post = Post(p=p,
                q=q,
                author=current_user,
                result=stringed_result,
                sigma2=sigma2,
                title=title,
                stderr=stde,
                description=description,
                is_private=is_private)
    db.session.add(post)
    db.session.commit()
    # Rename the file according to the post id
    os.rename(Config.DATASETS_FOLDER + filename, Config.DATASETS_FOLDER + str(post.id) + '.csv')
    generate_arma_plot_file(dataset, p, q, str(post.id))
    return post


# sim_data, p, q = simulate_arma_data(n_sample=1000, ar_params=np.array([]), ma_params=np.array([0.1, 0.3, 0.5]))
# print(maximize_likelihood(sim_data, 2, 2))

# df = pd.read_csv('^GSPC_m.csv')
# print(df['Close'])
# sep_data = df['Close'].to_numpy()
# print(maximize_likelihood(sim_data, p, q))


# def cov_us_us_function(a_d, a_pi, a_theta, sigma_d_d, sigma_d_pi, sigma_pi_pi, sigma_theta_d, sigma_theta_theta):
#     """Ornstein-Uhlenbeck Discretization of a continuous process."""
#     # Definition of the matrices A and Q of 'dZ(t) = AZ(t)+Qdw(t)'
#     matrix_a = sp.Matrix([[-a_d, 1, 0], [0, -a_pi, 0], [0, 0, -a_theta]])
#     matrix_q = sp.Matrix([[sigma_d_d, sigma_d_pi, 0], [0, sigma_pi_pi, 0], [sigma_theta_d, 0, sigma_theta_theta]])
#     print(matrix_a)
#     print(matrix_q)
#
#     # Our goal is to obtain the variance-covariance matrix of the stacked vector u_n^(s), that is:
#     # | Cov(u(p), u(p))  Cov(u(p), u(a)) |
#     # | Cov(u(a), u(p))  Cov(u(a), u(a)) |
#
#     delta_t = 1  # Notice: if daily data
#     tau = sp.Symbol('tau')
#
#     # Cov(u(p), u(p)) Note: \Delta t = 1 here
#     a_tau = sp.exp(- matrix_a * tau)
#     print(a_tau)
#
#     int_up_up = sp.exp(- matrix_a * tau) * matrix_q * matrix_q.transpose() * sp.exp(-matrix_a * tau).transpose()
#     cov_up_up = sp.exp(matrix_a) * sp.integrate(int_up_up, (tau, 0, delta_t)) * sp.exp(matrix_a).transpose()  # matrix_m
#
#     # Definition of useful matrices
#     r = sp.Symbol('r')
#     matrix_s = sp.exp((r * sp.eye(3) + matrix_a) * delta_t)
#     matrix_t = sp.exp(2 * r * sp.eye(3) * delta_t)
#     matrix_m1 = matrix_t * cov_up_up  # matrix_t * matrix_m
#     matrix_m2 = matrix_q * (matrix_q.transpose()) * (matrix_t - matrix_s.transpose()) * (
#         (r * sp.eye(3) - matrix_a.transpose()).inv())
#     matrix_m3 = (1 / 2 * r) * (matrix_t - sp.eye(3)) * matrix_q * (matrix_q.transpose())
#
#     # Cov(u(p), u(a))
#     cov_up_ua = sp.exp(-r * sp.eye(3) * delta_t) * (matrix_m1.transpose() - matrix_m2.transpose()) * (
#         (r * sp.eye(3) + matrix_a.transpose()).inv())
#
#     # Cov(u(a), u(p))
#     cov_ua_up = ((r * sp.eye(3) + matrix_a).inv()) * (sp.exp(-r * sp.eye(3) * delta_t)) * (matrix_m1 - matrix_m2)
#
#     # Cov(u(a), u(a))
#     cov_ua_ua = ((r * sp.eye(3) + matrix_a).inv()) * (matrix_m1 - matrix_m2.transpose() - matrix_m2 + matrix_m3) * (
#         (r * sp.eye(3) + matrix_a.transpose()).inv())
#
#     # The required output matrix is Cov(u(s), u(s))
#     cov_us_us = sp.Matrix([[cov_up_up, cov_up_ua], [cov_ua_up, cov_ua_ua]])
#     print(cov_us_us)
#
#
# # cov_us_us_function(1, 1, 1, 0.5, 0.3, 0.2, 0.8, 0.9)

def evaluate_fitting(n_sample, ar_params, ma_params, variance, size, library):
    nbins = math.ceil(sqrt(size))
    _, p, q = simulate_arma_data(n_sample, ar_params, ma_params, 12345, variance)
    all_results = np.zeros((1, p + q))
    for i in range(size):
        print(f"Test {i}")
        seed = np.random.randint(0, 100000)
        dataset, p, q = simulate_arma_data(n_sample, ar_params, ma_params, seed, variance)
        results, sigma2, _ = maximize_likelihood(data=dataset, p=p, q=q, library=library)
        all_results = np.vstack([all_results, results])
    plt.style.use('seaborn')
    num_plots = p + q
    total_cols = 3 if num_plots > 2 else num_plots
    total_rows = math.ceil(num_plots / total_cols)
    # fig, axs = plt.subplots(nrows=total_rows, ncols=total_cols, figsize=(7 * total_cols, 7 * total_rows),
    #                         constrained_layout=True)

    fig = plt.figure(figsize=(7 * total_cols, 7 * total_rows))
    for i in range(p + q):
        if i < p:
            psi_name = fr'$\phi_{i + 1}$'
            psi_value = ar_params[i]
        else:
            psi_name = fr'$\theta_{i - p + 1}$'
            psi_value = ma_params[i - p]
        ax = fig.add_subplot(total_rows, total_cols, i + 1)
        sns.kdeplot(all_results[:, i], color='magenta', ax=ax)
        ax.hist(all_results[:, i], bins=nbins)
        ax.axvline(x=psi_value, linestyle='--', color='red')
        ax.set_title(f"Estimation test for {psi_name} = {psi_value}")
    filename = token_hex(8)
    plt.savefig(Config.PLOTS_FOLDER + filename)
    return filename + ".png"


def stationary_initialization(F, Q):
    dimF = np.shape(F)[0]
    I_FkroF = np.eye(dimF * dimF) - np.kron(F, F)
    detI_FkroF = np.linalg.det(I_FkroF)
    if (detI_FkroF == 0) or (0 < detI_FkroF < 10 ^ (-6)):  # Pseudo inverse in case of singularity
        I_FkroFinv = np.linalg.pinv(I_FkroF, rcond=1e-15, hermitian=True)
    else:  # Standard inverse
        I_FkroFinv = np.linalg.inv(I_FkroF)
    vec_Q = Q.reshape(-1, 1)
    vec_P0 = np.dot(I_FkroFinv, vec_Q)
    P0_matrix = vec_P0.reshape(dimF, dimF)
    return P0_matrix

# def child_job(p, q, filename, title, description, is_private, username, library):
#     app = create_app()
#     with app.app_context():
#         user = User.query.filter_by(username=username).first_or_404()
#         print(user)
#
#         # Obtain the dataset from the file
#         dataset = np.genfromtxt(Config.DATASETS_FOLDER + filename, delimiter=',')
#         # Optimize parameters
#         result, sigma2 = maximize_likelihood(data=dataset, p=p, q=q, library=library)
#         stringed_result = ','.join(str(x) for x in result)
#         # Add results to the database
#         post = Post(p=p,
#                     q=q,
#                     author=user,
#                     result=stringed_result,
#                     sigma2=sigma2,
#                     title=title,
#                     description=description,
#                     is_private=is_private)
#
#         print(post)
#         db.session.add(post)
#         db.session.commit()
#
#         # Rename the file according to the post id
#         os.rename(Config.DATASETS_FOLDER + filename, Config.DATASETS_FOLDER + str(post.id) + '.csv')
#         generate_arma_plot_file(dataset, p, q, str(post.id))
