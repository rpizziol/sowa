from secrets import token_hex
from flask import Blueprint, send_file, abort, flash
from flask import render_template, url_for, redirect
from flask_login import current_user, login_required

import concurrent.futures

from sowa.models import Post
from sowa.posts.forms import DatasetForm, AnalysisForm, EvaluateEstimationsForm
from sowa.posts.controller import delete_post_controller, generate_dataset_controller, estimations_evaluator_controller
from sowa.config import Config
from sowa.posts.utils import execute_analysis

posts = Blueprint('posts', __name__)


@posts.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)  # If it doesn't exist: error 404
    if post.is_private and post.author != current_user:
        abort(403)
    return render_template('posts/post.html', title=post.id, post=post)


@posts.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    delete_post_controller(post_id)
    return redirect(url_for('main.history'))


@posts.route("/dataset", methods=['GET', 'POST'])
@login_required
def generate_dataset():
    form = DatasetForm()
    if form.validate_on_submit():
        dataset_fn = generate_dataset_controller(form)
        return redirect(url_for('posts.download_dataset', filename=dataset_fn))
    return render_template('posts/dataset_generator.html', title='Create ARMA Dataset', form=form,
                           legend='Create ARMA Dataset')


@posts.route("/download-dataset/<string:filename>/")
@login_required
def download_dataset(filename):
    """
    Download a newly generated dataset.
    :param filename:    The name of the csv file to download. (without extension)
    :return:
    """
    return send_file('static/datasets/' + filename + '.csv', as_attachment=True)  # , attachment_filename=filename)


@posts.route("/estimation", methods=['GET', 'POST'])
@login_required
def estimate_parameters():
    form = AnalysisForm()
    if form.validate_on_submit():
        print(current_user)
        filename = token_hex(8) + '.csv'  # Generate a new filename
        form.dataset.data.save(Config.DATASETS_FOLDER + filename)
        # with concurrent.futures.ThreadPoolExecutor() as executor:
        #     executor.submit(child_job, form.p.data, form.q.data, filename, form.title.data,
        #                     form.description.data, form.is_private.data, current_user.username, form.library.data)
        # flash("Your request has been processed. Check your posts again.", category='success')
        post = execute_analysis(p=form.p.data, q=form.q.data, library=form.library.data, title=form.title.data,
                         description=form.description.data, is_private=form.is_private.data, filename=filename)
        return redirect(url_for('posts.post', post_id=post.id))
    # thread = threading.Thread(target=execute_analysis, args=[form, filename])
    # thread.start()
    # post = execute_analysis(form, filename)
    return render_template('posts/analyser.html', title='New Analysis', form=form, legend='New Analysis')


@posts.route("/fitting-evaluator", methods=['GET', 'POST'])
@login_required
def estimations_evaluator():
    form = EvaluateEstimationsForm()
    if form.validate_on_submit():
        plot_fn = estimations_evaluator_controller(form)
        return render_template('posts/estimations_evaluator.html', title='Evaluate Estimations', form=form,
                               legend='Evaluate Estimations', plot_fn=plot_fn)
    return render_template('posts/estimations_evaluator.html', title='Evaluate Estimations', form=form,
                           legend='Evaluate Estimations')
