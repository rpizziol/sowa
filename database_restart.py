from sowa import db
from sowa import create_app

app = create_app()
app.app_context().push()

db.create_all()
